/* HTML DOM sanitizer */
import DOMPurify from "dompurify";
/* set it as global */
window.Sanitizer = DOMPurify;

/* so we can connect p2p to edit a document */
import "@sctlib/rtc";

/* custom elements */
import SpaceElement from "./components/space-element.js";
import SpaceSite from "./components/space-site.js";
import SpaceEdit from "./components/space-edit.js";
import SpaceQrcode from "./components/space-qrcode.js";

const componentDefinitions = {
	"space-element": SpaceElement,
	"space-site": SpaceSite,
	"space-edit": SpaceEdit,
	"space-qrcode": SpaceQrcode,
};

export function defineComponents(components = {}) {
	Object.entries(components).map(([cTag, cDef]) => {
		if (!customElements.get(cTag)) {
			customElements.define(cTag, cDef);
		}
	});
}

defineComponents(componentDefinitions);

export default componentDefinitions;
