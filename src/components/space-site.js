import { unified } from "unified";
import remarkParse from "remark-parse";
import remarkRehype from "remark-rehype";
import rehypeRaw from "rehype-raw";
import rehypeStringify from "rehype-stringify";
import rehypeSanitize, { defaultSchema } from "rehype-sanitize";
import "@sctlib/matrix-room-element";

const sanitizerConfig = {
	CUSTOM_ELEMENT_HANDLING: {
		tagNameCheck: (tagName) => {
			return tagName.match(/matrix-room-element/) || tagName.match(/iframe/);
		},
		attributeNameCheck: (attr) => {
			return (
				attr.match(/profile-id/) ||
				attr.match(/event-id/) ||
				attr.match(/show-context/) ||
				attr.match(/show-event-info/) ||
				attr.match(/show-event-actions/) ||
				attr.match(/show-space-children/)
			);
		},
		// allow customized built-ins
		allowCustomizedBuiltInElements: true,
	},
};

export default class SpaceSite extends HTMLElement {
	static get observedAttributes() {
		return ["value", "site-title"];
	}
	get value() {
		return JSON.parse(this.getAttribute("value"));
	}
	get siteTitle() {
		return this.getAttribute("site-title") || "";
	}
	async attributeChangedCallback(name, oldValue, newValue) {
		if (name === "value" && this.$root) {
			await this.render();
			this.hashchange();
		}
	}
	constructor() {
		super();
		this.setAttribute("tabindex", 0);
	}
	async connectedCallback() {
		this.$root = this

		/* When URL changes. */
		window.addEventListener("hashchange", this.hashchange.bind(this));
		/* When on the URL already, perhaps after scrolling, and clicking again, which doesn’t emit `hashchange`. */
		this.addEventListener("click", this.hashchangeClick.bind(this), false);

		/* render the space/page/content */
		await this.render();
		/* trigger initial URL-hash changed after render */
		this.hashchange();
	}
	disconnectedCallback() {
		window.removeEventListener("hashchange", hashchange);
		this.removeEventListener("click", hashchange);
	}
	async render() {
		if (this.value) {
			this.$root.innerHTML = await this.formatValue(this.value);
		} else {
			this.$root.innerText = "";
		}
		this.updatePageTitle(this.$root.innerText);
	}
	updatePageTitle(value) {
		if (!value) {
			document.title = this.siteTitle;
		} else {
			/* first new line, first 50 chars */
			const titleFragment = value.substr(0, 50).split("\n")[0];
			document.title = `${titleFragment} @ ${this.siteTitle}`;
		}
	}
	async formatValue(value) {
		const file = await unified()
			.use(remarkParse)
			.use(remarkRehype, { allowDangerousHtml: true })
			.use(rehypeRaw)
			.use(rehypeStringify)
			.use(rehypeSanitize, {
				...defaultSchema,
				tagNames: [
					...defaultSchema.tagNames,
					"style",
					"section",
					"header",
					"aside",
					"footer",
					"matrix-room-element",
					"iframe",
				],
				attributes: {
					...defaultSchema.attributes,
					"matrix-room-element": [
						"profile-id",
						"show-context",
						"show-event-info",
						"show-event-actions",
						"show-space-children",
					],
				},
			})
			.process(value);
		const val = Sanitizer.sanitize(String(file), sanitizerConfig);
		return val;
	}
	/*
		 a function to check if the URL-hash has changed (part after the `#` char in URL);
		 needed because we sanitize `id` attributes for user generated content
		 https://unifiedjs.com/explore/package/rehype-sanitize/#example-headings-dom-clobbering
	 */
	hashchange() {
		/** @type {string|undefined} */
		let hash;

		try {
			hash = decodeURIComponent(location.hash.slice(1));
		} catch {
			return;
		}

		const name = "user-content-" + hash;
		const selector = `[id="${name}"]`;
		const target =
			this.$root.querySelector(selector) || this.$root.querySelector(selector);

		if (target) {
			setTimeout(() => {
				target.scrollIntoView();
			}, 0);
		}
	}
	hashchangeClick(event) {
		if (
			event.target &&
			event.target instanceof HTMLAnchorElement &&
			event.target.href === location.href &&
			location.hash.length > 1
		) {
			setTimeout(() => {
				if (!event.defaultPrevented) {
					this.hashchange();
				}
			});
		}
	}
}
