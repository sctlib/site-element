import "webcomponent-qr-code";

/* These styles are only for the `qr-code.shadowroot` */
const css = `
	svg {
		width: 100%;
		max-height: 100%;
		height: auto;
	}
`;

export default class SpaceQRCode extends HTMLElement {
	static get observedAttributes() {
		return ["value"];
	}
	get value() {
		return this.getAttribute("value");
	}
	attributeChangedCallback(name, oldValue, newValue) {
		if (name === "value" && newValue && this.$qrcode) {
			this.render(newValue);
		}
	}
	get clientUrl() {
		const {host,pathname} = window.location
		return `${host}${pathname ? pathname : ''}`
	}
	connectedCallback() {
		/* the qr code */
		this.$qrcode = document.createElement("qr-code");
		this.$qrcode.addEventListener("click", this.handleQRClick.bind(this));
		this.$qrcode.setAttribute("format", "svg");
		this.$qrcode.setAttribute("modulesize", "5");

		/* can be tabbed with keyboard */
		this.$qrcode.setAttribute("tabindex", "0");

		/* the copy input */
		this.$buttonCopy = document.createElement("button");
		this.$buttonCopy.innerText = "Copy URL";
		this.$buttonCopy.title =
			"Copy to the clipboard, the URL address of the current page (the URL also contains all the content).";
		this.$buttonCopy.addEventListener("click", this.copyURL.bind(this));

		/* the button to make an archive of all content+URL */
		this.$buttonArchive = document.createElement("button");
		this.$buttonArchive.innerText = "→archive.is?URL=content⚠";
		this.$buttonArchive.title = "Archive the entire content by its URL to archive.is (WARNING: this is permanent)";
		this.$buttonArchive.addEventListener("click", this.archiveURL.bind(this));

		/* button to list all archives */
		this.$buttonListArchives = document.createElement("a");
		this.$buttonListArchives.innerText = `←archive.is?url=${this.clientUrl}`;
		this.$buttonListArchives.title = `Retrive all archived ${this.clientUrl} data`;
		this.$buttonListArchives.href = `https://archive.is/${this.clientUrl}`;

		/* menu */
		const $menu = document.createElement('menu');
		[this.$qrcode, this.$buttonCopy, this.$buttonArchive, this.$buttonListArchives].forEach(($el) => {
			const $li = document.createElement('li');
			$li.append($el);
			$menu.append($li);
		});

		/* insert in dom */
		this.append($menu);

		/* render value as attribute */
		if (this.value) {
			this.render(this.value);
		}
	}

	render(value) {
		/* update the value of the qr code */
		this.$qrcode.setAttribute("data", value);
		/* add custom styles to the shadow root,
			 after we update data value, or it overwrittes them */
		const $styles = document.createElement("style");
		$styles.innerHTML = css;
		this.$qrcode.shadowRoot.appendChild($styles);
	}
	copyURL({ target }) {
		/* copy current URL to clipboard */
		navigator.clipboard.writeText(this.value);

		/* set is-active atr on element for styles */
		target.setAttribute("is-active", true);
		window.setTimeout(() => {
			target.removeAttribute("is-active");
		}, 600);
	}
	archiveURL({target}) {
		window.open(`https://archive.is?url=${encodeURIComponent(this.value)}`)
	}
	handleQRClick() {
		/* const svg = this.$qrcode.shadowRoot.querySelector('svg') */
		/* window.open(`data:image/svg+xml;utf8,${svg}`) */
		if (this.getAttribute("is-active") === "true") {
			this.removeAttribute("is-active");
		} else {
			this.setAttribute("is-active", true);
		}
	}
}
