export default class SpaceEdit extends HTMLElement {
	static get observedAttributes() {
		return ["value", "max-length"];
	}
	get value() {
		return JSON.parse(this.getAttribute("value"));
	}
	get maxLength() {
		const qrCodeMaxChars = 2953;
		return this.getAttribute("max-length") || "1871";
	}
	attributeChangedCallback(name, oldValue, newValue) {
		if (name === "value" && newValue && this.$textArea) {
			this.render();
		}
	}
	connectedCallback() {
		this.$textArea = document.createElement("textarea");
		this.$textArea.addEventListener("input", this.onInput.bind(this));
		this.$textArea.placeholder = "Write a message to be shared";
		this.$textArea.maxLength = this.maxLength;
		this.append(this.$textArea);
		this.render();
		this.$textArea.focus();
	}
	render() {
		if (this.value) {
			this.$textArea.value = this.value;
		} else {
			this.$textArea.value = "";
		}
	}
	onInput(event) {
		this.$textArea.value = event.target.value;
	}
}
