import spaceElementStyles from "../styles/space-element.css?inline";
import * as Y from "yjs";
import debounce from "lodash.debounce";

const template = document.createElement("template");
template.innerHTML = `
	<style>
		${spaceElementStyles}
	</style>
	<slot name="site">
		<space-site tabindex="0"></space-site>
	</slot>
	<slot name="edit">
		<space-edit></space-edit>
	</slot>
	<slot name="settings">
		<details>
			<summary title="Menu"></summary>
			<space-element-menu>
				<menu>
					<li>
						<space-qrcode></space-qrcode>
					</li>
					<li><a target="_blank" href="https://gitlab.com/sctlib/goog-space" title="About this website?">About</a></li>
					<li><button type="button" name="edit" title="Edit this space's content">Edit</button></li>
					<li>
						<rtc-user
								search-params="['matrix-peers', 'peer-data', 'signaling-method']"
							></rtc-user>
					</li>
				</menu>
			</space-element-menu>
		</details>
	</slot>
`;

/**
 * Unicode to ASCII (encode data to Base64)
 * @param {string} data
 * @return {string}
	 https://base64.guru/developers/javascript/examples/unicode-strings
	 It preserves all unicodes characters (including emojis) when in a URL parameter
 */
function decode(data) {
	return decodeURIComponent(escape(atob(data)));
}
function encode(data) {
	return btoa(unescape(encodeURIComponent(data)));
}

export default class SpaceElement extends HTMLElement {
	static get observedAttributes() {
		return ["type", "is-editing", "space-title", "space-description"];
	}
	get type() {
		return this.getAttribute("type") || "default";
	}
	get isEditing() {
		return this.getAttribute("is-editing") === "true";
	}
	set isEditing(bool) {
		this.setAttribute("is-editing", bool);
	}
	get siteTitle() {
		return this.getAttribute("space-title") || "goog.space";
	}
	get siteDescription() {
		return this.getAttribute("space-description") || "spaces for all";
	}
	get debounceTime() {
		return 333;
	}
	/* apply local changes and send updates */
	updateYDoc(data = "") {
		this.yDoc.transact(() => {
			this.yData.delete(0, this.yData.toString().length);
			this.yData.insert(0, data);
		});
	}
	updateRtc() {
		const yUpdate = Y.encodeStateAsUpdate(this.yDoc);
		if (this.$rtcUser.dataChannel) {
			this.$rtcUser.send(yUpdate);
		}
	}
	updateUrl(data) {
		const { search } = window.location;
		const params = new URLSearchParams(search);
		if (data.value) {
			params.set("data", encode(JSON.stringify(data)));
			history.replaceState(null, this.siteTitle, "#" + params.toString());
		} else {
			params.delete("data");
			if (params.size) {
				history.replaceState(null, this.siteTitle, "#" + params.toString());
			} else {
				history.pushState(
					"",
					this.siteTitle,
					window.location.pathname + window.location.search
				);
			}
		}
	}
	readDataURL() {
		const { search, hash } = window.location;
		const hashParams = new URLSearchParams(hash.slice(1));
		const hashData = hashParams.get("data");
		const hashInput = hashParams.get("input");

		let data;
		if (hashData) {
			data = JSON.parse(decode(hashData));
		} else if (hashInput) {
			data = {
				type: "text",
				value: hashInput,
			};
		}

		if (!data) {
			const searchParams = new URLSearchParams(search);
			const searchData = searchParams.get("data");
			const searchInput = searchParams.get("input");
			if (searchData) {
				data = JSON.parse(decode(searchData));
			} else if (searchInput) {
				data = {
					type: "text",
					value: searchInput,
				};
			}
		}
		return data;
	}

	constructor() {
		super();
		this.setAttribute("tabindex", 0);
		this.data = {};
		this.$root = this.attachShadow({ mode: "open" });
		this.$root.appendChild(template.content.cloneNode(true));
		this.$root.addEventListener("keydown", this.onKeyboard.bind(this));
		this.$modal = this.$root.querySelector("details");
		this.$site = this.$root.querySelector('slot[name="site"] space-site');
		// this.$site.addEventListener('dblclick', this.onSiteDoubleClick.bind(this))
		this.$site.setAttribute("space-title", this.siteTitle);
		this.$edit = this.$root.querySelector('slot[name="edit"] space-edit');
		this.$edit.addEventListener("input", this.onInput.bind(this));
		this.$edit.addEventListener("dblclick", this.onSiteDoubleClick.bind(this));
		this.$qrcode = this.$root.querySelector(
			'slot[name="settings"] space-qrcode'
		);
		const $editBtn = this.$root.querySelector(
			'slot[name="settings"] button[name="edit"]'
		);
		$editBtn.addEventListener("click", this.onEdit.bind(this));

		this.$rtcUser = this.$root.querySelector('slot[name="settings"] rtc-user');
		this.$rtcUser.addEventListener(
			"dataChannel",
			this.onDataChannel.bind(this)
		);
		this.dataChannels = [];
		this.$rtcUser.addEventListener(
			"channelMessage",
			this.onChannelMessage.bind(this)
		);
		// have to debounce setting the URL params
		this.debouncedUpdateUrl = debounce(this.updateUrl, this.debounceTime);
	}
	connectedCallback() {
		this.data = this.readDataURL();
		this.yDoc = new Y.Doc();
		this.yData = this.yDoc.getText("data", Y.XmlText);
		/* this.yDoc.on("update", this.onYDocUpdate.bind(this)); */
		this.yDoc.on("afterTransaction", this.onYDocUpdate.bind(this));
		if (this.data) {
			this.isEditing = false;
			this.yData.insert(0, this.data.value);
		} else if (!this.data) {
			this.isEditing = true;
		}
		this.renderMeta();
		this.render();
		this.focus();
	}
	renderMeta() {
		const $head = document.querySelector("head");

		let $title = document.querySelector("title");
		if (!$title) {
			$title = document.createElement("title");
			$head.append($title);
		}
		$title.innerText = this.siteTitle;

		const $desc = document.createElement("meta");
		$desc.setAttribute("name", "description");
		$desc.setAttribute("content", this.siteDescription);
		$head.append($desc);
	}
	render() {
		if (this.data) {
			this.renderData(this.data.value);
		} else if (!this.data) {
			this.renderData("");
		}
		this.renderUrlSharing();
	}
	renderData(value) {
		if (value) {
			this.$site.setAttribute("value", JSON.stringify(value));
			this.$edit.setAttribute("value", JSON.stringify(value));
		} else {
			this.$site.removeAttribute("value");
			this.$edit.removeAttribute("value");
		}
	}
	renderUrlSharing(value = window.location) {
		if (value) {
			this.$qrcode.setAttribute("value", value);
		}
	}

	onYDocUpdate() {
		const data = {
			value: this.yData.toString(),
		};
		this.debouncedUpdateUrl(data);
		this.renderUrlSharing();
		this.renderData(data.value);
	}
	onInput(event) {
		this.updateYDoc(event.target.value);
		this.updateRtc();
	}
	onPeerInput(yString) {}
	onEdit(event) {
		this.isEditing = !this.isEditing;
		if (this.isEditing) {
			this.$edit.querySelector("textarea").focus();
		} else {
			this.$site.focus();
		}
	}
	onSiteDoubleClick(event) {
		this.onEdit();
	}
	onKeyboard(event) {
		/* on escape, close modal, or cancel editting */
		if (event.key === "Escape") {
			if (this.$modal.open) {
				this.$modal.open = false;
			} else {
				if (this.isEditing) {
					this.onEdit();
				}
			}
		}
		if (event.key === "Enter" && event.ctrlKey) {
			this.onEdit();
		}
	}
	/* send data to peer (both do the same, to share their initial state) */
	onDataChannel(event) {
		this.updateRtc();
	}
	/* update our yDoc data with the peer data, syncs all states on changes */
	onChannelMessage({ detail }) {
		let reader = new FileReader();
		reader.onloadend = () => {
			const update = new Uint8Array(reader.result);
			Y.applyUpdate(this.yDoc, update);
		};
		reader.readAsArrayBuffer(detail);
	}
}
