# space-element (goog.space)

> Generate a website, which content (a combination of: plaintext,
> markdown, HTML) are stored in the URL (as encoded data), and shared
> as QR code (pointing to the goog.space instance; and having the data in the URL).

An instance of this webcomponent is available at [goog.space](https://goog.space).

## Features

- content can be in markdown
- content can contain HTML elements, and `<style></style>` tags
- change to the content generates a new QR-code linking to the content
- the URL contain all the content of the page (the QR-code therefore also does)
- the `<space-element/>` is a web component container the logic
- content is sanitized (twice, with DOMpurify && rehypeSanitize)
- some "web components are white listed" (`<matrix-room-element/>`
  [see docs](https://sctlib.gitlab.io/mwc/) , `<qr-code/>` etc.)

It also allows, experimental, and maybe not here to stay:
- [p2p WebRTC](https://gitlab.com/sctlib/rtc) connection
- [yjs document](https://github.com/yjs/yjs) editing

The RTC code (ours) has dependency to matrix-room-element as well
(code is currently in double, nee to work with peer
dependencies). Also the RTCCode and QRCode have a dependency to wasm
mode `zbar` to decode the qrcodes from images/videos (and that is
heavy). So we gotta reflect on the trade offs.

## Examples

In all examples, the "data of the site", is stored 100% in the URL (no server needed). This is why the URLs are so long!

- a basic page:

https://goog.space/#data=eyJ0eXBlIjoidGV4dCIsInZhbHVlIjoiIyBIZWxsbyB3b3JsZCFcblxuVGhpcyBpcyBhIGRlZmF1bHQgcGFnZSwgd2l0aCBjb250ZW50IG9ubHkhXG5cbiMjIFVzYWdlXG5cbi0gdGhlcmUgaXMgYSBcIm1lbnVcIiBidXR0b24gKHdlYnNpdGUgbmF2aWdhdGlvbiksIGF0IHRoZSBib3R0b20gb2YgdGhlIHBhZ2Vcbi0gZG91YmxlIGNsaWNrL3RhZyB0aGUgcGFnZSB0byBnbyBpbnRvIGVkaXQgbW9kZSwgb3Igb3V0IG9mIGl0IChpbnRvIFwicmVuZGVyXCIgbW9kZSlcbi0geW91IGNhbiBtYWtlIGEgcHJpbnQgdmVyc2lvbiBvZiB0aGUgcGFnZSAocHJpbnQgcGFnZSB0byBwZGYgaW4geW91ciBicm93c2VyKTsgaWYgeW91IG9wZW4gdGhlIG1lbnUsIHRoZSBxciBjb2RlIHdpbGwgYmUgcHJpbnRlZCB0b28hXG5cbiMjIE1hcmtkb3duXG5cbi0gbWFya2Rvd24gZm9ybWF0ICoqaXMgc3VwcG9ydGVkIGluIHRoZSBwYWdlKipcbi0gYXMgd2VsbCBhcyBIVE1MIGluc2lkZSB0aGUgbWFya2Rvd25cbi0gcG9zc2libGUgdG8gbWFrZSBsaW5rcyEgW2dvb2cuc3BhY2VdKGdvb2cuc3BhY2UpXG5cbiMjIFRoZXJlIGlzIG1vcmUhXG5cbi0gW3Zpc2l0IHRoZSBwcm9qZWN0J3MgY29kZSByZXBvc2l0b3J5XShodHRwczovL2dpdGxhYi5jb20vc2N0bGliL3NwYWNlLWVsZW1lbnQpXG5cblxuPiBUaGFua3MgZm9yIHJlYWRpbmchIn0%3D

- a page with HTML and styles

https://goog.space/#data=eyJ0eXBlIjoidGV4dCIsInZhbHVlIjoiIyBNeSB3ZWJzaXRlXG5cbjxoZWFkZXI%2BXG5cbldlbGNvbWUgdG8gbXkgd2VicGFnZSFcblxuWW91IGNhbiBhY3R1YWxseSBlZGl0IGl0LCBhbmQgbWFrZSBpdCB5b3VyIG93bi5cblxuSnVzdCBkb3VibGUgY2xpY2sgdGhlIHBhZ2UsIG9yIGNsaWNrIHRoZSBtZW51IGJ1dHRvbiBhdCB0aGUgYm90dG9tIGNlbnRlciBvZiB0aGlzIHBhZ2UuXG5cbjwvaGVhZGVyPlxuXG5cbjxkZXRhaWxzPlxuPHN1bW1hcnk%2BV2FudCB0byBzdGFydCBhIHdlYnNpdGUgZm9yIHlvdXJzZWxmPyE8L3N1bW1hcnk%2BXG5cbldlbGwgdGhlbjpcbi0gdmlzaXQgW2dvb2cuc3BhY2VdKGdvb2cuc3BhY2UpXG4tIHN0YXJ0IHR5cGluZyBjb250ZW50XG4tIHNoYXJlIHRoZSBVUkwgb2YgdGhlIHBhZ2UsIG9yIHRoZSBRUiBjb2RlIHRvIHlvdXIgY29tbXVuaXRpZXNcbi0geW91IGNhbiBhbHNvIHVzZSBhIFtcImxpbmsgbWluaWZpZXIgc2VydmljZV0oaHR0cHM6Ly9iaXRseS5jb20pLCB0byBzaGFyZSBhIGxpbmsgeW91IGhhdmUgY29udHJvbCBvbiAoYW5kIGNhbiB1cGRhdGUhKSBcbjwvZGV0YWlscz5cblxuXG48c3R5bGU%2BXG4vKiB0aGUgOmhvc3QgcmVmZXJlbmNlcywgaW4gQ1NTLCB0aGUgd2ViLWNvbXBvbmVudCB0b3AgcGFyZW50IHRoYXQgY2FuIGJlIGVkaXRlZCAqL1xuOmhvc3Qge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcbn1cbmgxIHtcbiAgZm9udC1zaXplOiAxMHZoO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5oZWFkZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5zcGFjZS1zaXRlIHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG48L3N0eWxlPiJ9

## Import in a site

If you would like to use this project on your own website, it is possible to import the project from npm or a cdn

```html
<script src="https://unpkg.com/@sctlib/space-element" type="module"></script>
<space-element></space-element>
<style>
	/* for a full height space-element */
	html,
	body {
		display: flex;
		flex-direction: column;
		flex-grow: 1;
	}
	html {
		min-height: 100%;
	}
</style>
```

https://goog.space/#data=eyJ0eXBlIjoidGV4dCIsInZhbHVlIjoiIyBFeGFtcGxlIHVzYWdlIGluIGEgSFRNTCBmaWxlLlxuXG4tIGltcG9ydCB0aGUgc2NyaXB0XG4tIGluc2VydCB0aGUgYDxzcGFjZS1lbGVtZW50Lz5gIHdlYiBjb21wb25lbnRcbi0gc3R5bGUgdGhlIHN1cnJvdW5kaW5nIGFwcGxpY2F0aW9uLCB0byBnaXZlIGl0IHNwYWNlIHRvIGdyb3dcblxuYGBgaHRtbFxuPHNjcmlwdCBzcmM9XCJodHRwczovL3VucGtnLmNvbS9Ac2N0bGliL3NpdGUtZWxlbWVudFwiIHR5cGU9XCJtb2R1bGVcIj48L3NjcmlwdD5cbjxzcGFjZS1lbGVtZW50Pjwvc3BhY2UtZWxlbWVudD5cbjxzdHlsZT5cbiAgaHRtbCwgYm9keSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGZsZXgtZ3JvdzogMTtcbiAgfVxuICBodG1sIHtcbiAgICBtaW4taGVpZ2h0OiAxMDAlO1xuICB9XG48L3N0eWxlPlxuYGBgIn0%3D

# Issues

With gitlab pages hosting, we get a http error 401, Request URL is too long (so we host on cloudflare pages).

# URL APIs

- `#input=` with a string of text as input
- `#data=` with a goog.space encoded data (see #data-encoding)

# About
## `goog`
The `goog`(ol) is a [large number (wikipedia)](https://en.wikipedia.org/wiki/Googol).

## the data encoding

The encoding is here to stay, and **should never change** to keep the
"old URL data" decodable.

From the `space-element.js` code:

```js
/**
 * Unicode to ASCII (encode data to Base64)
 * @param {string} data
 * @return {string}
	 https://base64.guru/developers/javascript/examples/unicode-strings
	 It preserves all unicodes characters (including emojis) when in a URL parameter
 */
function decode(data) {
	return decodeURIComponent(escape(atob(data)));
}
function encode(data) {
	return btoa(unescape(encodeURIComponent(data)));
}
```


If this one day changed, we should keep backward compatibility with
"migrations", but hoping we never have to.
## app minimalism
Somehow, maybe this application should be rewritten with a `<body
contenteditable/>`, and `edit.goog.space` subdomain, so the resulting
HTML is as clean as possible (and archive.is or archive.org can almost
have the "raw user content" withtout any goog.space content).
## archiving an URL to archive.is

Archive.is is free, works from the URL, is super practical, works with
goog.space (archive.org had troubles with web components, and very slow URLs).

- https://mementoweb.org/
- many other services follow memento protocol https://mementoweb.org/depot/
## sharing a space-element goog-space to matrix
## mime types & URL Search Params
The application, as "simple" as it is, tries to define the inputs and
outputs it can accept and provide, and its mime type "as a protocol".

The idea is to somehow get a clear and predictable pattern to
communicate with the URL API of a goog-space application.

The `protocol` is `space.goog` following the JAVA (and Matrix.org;
reversed domain name) naming conventions, so it can be written
`space.goog:` (as in `new URL().protocol` format).

Goog.space accepts currently two forms of input, "unencode text data"
and "raw text data".

- the `#input=` url search (hash) param for user text, e.g.: `hello world`.
- the `#data=` for the encoded data, decode to the user text, e.g.:
  `eyJ2YWx1ZSI6ImhlbGxvIHdvcmxkIn0%3D`
	
> For more information on them see the rest of the documentation.

We could therefore imagine representing the user `input` URL content
(in a JSON "event file" format) as:

```json
{
  "mimetype": "space.goog://input+text",
  "space.goog://input+text": "hello world"
}
```

As for the encoded `data` representation, since its decoded result
infered from the mimetype URL schema "space.goog:" of type "data",
threfore encoded:

```json
{
  "mimetype": "space.goog://data+json",
  "space.goog://data+json": "eyJ2YWx1ZSI6ImhlbGxvIHdvcmxkIn0%3D"
}
```

> For `encoding` and `decoding` functions, see docs section.


> Note: the "file event" representation is an inspired variant of a
> [matrix event
> specification](https://www.ietf.org/id/draft-ralston-mimi-matrix-message-format-01.html)
